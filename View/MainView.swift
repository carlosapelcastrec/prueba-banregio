//
//  MainView.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

//Se agrega liga de gitlab
//https://gitlab.com/carlosapelcastrec/prueba-banregio

import SwiftUI

struct MainView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \DebitsCD.idCustomer, ascending: true)],
        animation: .default)
    private var debits: FetchedResults<DebitsCD>
    private var dataVM = DataViewModel()
    
    var body: some View {
        VStack{
            Text("Cuentas Activas con Saldos Pendientes")
            List {
                ForEach(debits) { debit in
                    NavigationLink {
                        Text("Item at")
                    } label: {
                        Text(debit.idCustomer ?? "")
                    }
                }
            }
        }
    }
}


struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
