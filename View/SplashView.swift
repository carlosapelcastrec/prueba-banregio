//
//  SplashView.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
       Image("logo")
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
