//
//  InitView.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//
//Se agrega liga de gitlab
//https://gitlab.com/carlosapelcastrec/prueba-banregio

import SwiftUI

struct InitView: View {
    @State var isActive:Bool = false
    private var dataVM = DataViewModel()
    @Environment(\.managedObjectContext) private var viewContext
    
    var body: some View {
        VStack {
            if self.isActive {
                MainView()
            } else {
                SplashView()
            }
        }
        .onAppear {
            dataVM.validateData(context: viewContext)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                withAnimation {
                    self.isActive = true
                }
            }
        }
    }
}

struct InitView_Previews: PreviewProvider {
    static var previews: some View {
        InitView()
    }
}
