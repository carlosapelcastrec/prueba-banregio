//
//  Loan.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

import Foundation

struct Loan : Codable{
    var id : Int
    var date : String
    var amount : Double
    var status : String
}
