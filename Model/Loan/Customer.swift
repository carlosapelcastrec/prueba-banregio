//
//  Customer.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

import Foundation

struct Customer :Codable{
    let number : String
    let loan : [Loan]
}
