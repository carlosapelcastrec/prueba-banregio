//
//  Debit.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

import Foundation

struct Debit : Codable{
    var idCustomer : String
    var amount : Double
    var status : String
}
