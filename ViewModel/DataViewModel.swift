//
//  DataViewModel.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.
//

import Foundation
import CoreData
import SwiftUI

class DataViewModel {
    private var customers = [Customer]()
    private var debits = [Debit]()
    
    func validateData(context : NSManagedObjectContext){
        var isEmpty: Bool {
            do {
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerCD")
                let count  = try context.count(for: request)
                return count == 0
            } catch {
                return true
            }
        }
        if isEmpty{
            loadDataLoan(context: context)
            loadDataDebit(context: context)
        }
    }
    
    func loadDataLoan(context : NSManagedObjectContext){
        if let url = Bundle.main.url(forResource: "loan", withExtension: "json"),
           let data = try? Data(contentsOf: url) {
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let result = try decoder.decode(DataLoan.self, from: data)
                customers = result.customer
                
                
                for customer in customers {
                    saveLoan(customer: customer, context: context)
                }
                
                do {
                    try context.save()
                }
                catch {
                    print("Error al guardar objecto")
                }
            } catch {
                print("Error: Cannot convert data to JSON object\(error)")
            }
        }
    }
    
    func saveLoan(customer : Customer, context :  NSManagedObjectContext){
        let customerCD = CustomerCD(context: context)
        
        customerCD.number = customer.number
        for loan in customer.loan{
            customerCD.loans?.id = Int32(loan.id)
            customerCD.loans?.date = loan.date
            customerCD.loans?.amount = loan.amount
            customerCD.loans?.status = loan.status
        }
        
        do {
            try context.save()
            
        }
        catch {
            print("Error al guardar objecto")
        }
    }
    
    func loadDataDebit(context : NSManagedObjectContext){
        if let url = Bundle.main.url(forResource: "debit", withExtension: "json"),
           let data = try? Data(contentsOf: url) {
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let result = try decoder.decode(DataDebit.self, from: data)
                debits = result.debits
                
                for debit in debits {
                    saveDebit(debit: debit, context: context)
                }
            } catch {
                print("Error: Cannot convert data to JSON object\(error)")
            }
        }
    }
    
    
    func saveDebit(debit : Debit, context :  NSManagedObjectContext){
        let debitCD = DebitsCD(context: context)
        
        debitCD.idCustomer = debit.idCustomer
        debitCD.amount = debit.amount
        debitCD.status = debit.status
        
        do {
            try context.save()
            
        }
        catch {
            print("Error al guardar objecto")
        }
    }
}
