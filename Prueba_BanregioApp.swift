//
//  Prueba_BanregioApp.swift
//  Prueba_Banregio
//
//  Created by carlos on 06/08/22.

//Se agrega liga de gitlab
//https://gitlab.com/carlosapelcastrec/prueba-banregio


import SwiftUI

@main
struct Prueba_BanregioApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            InitView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
